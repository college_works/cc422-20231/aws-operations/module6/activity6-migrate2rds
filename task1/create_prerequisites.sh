#!/bin/bash
#
# Student: Lozano Gustavo - 20161317K
#
# This script automate the following subtask:
#  - "Task 1.3: Create prerequisite components"


source vars.sh

SECURITY_GROUP_NAME="MomPopCafeDatabaseSG"
SECURITY_GROUP_DESCRIPTION="Security group for Mom Pop Cafe database"
PRIVATE_SUBNETS_NAMES=('MomPopCafeDB Private Subnet 1'
                        'MomPopCafeDB Private Subnet 2')
PRIVATE_SUBNETS_CIDRS=('10.200.2.0/23' '10.200.10.0/23')
SUBNET_GROUP_NAME="MomPopCafeDB-Subnet-Group"
SUBNET_GROUP_DESCRIPTION="DB subnet group for Mom & Pop Cafe"


# Getting VPC id
vpcId=$(aws ec2 describe-vpcs \
          --filter "Name=tag:Name,Values=$VPC_NAME" \
          --query "Vpcs[*].VpcId" \
          --output text)

echo "$VPC_NAME vpc: $vpcId"

# Getting database security group ID
sgId=$(aws ec2 describe-security-groups \
        --filters "Name=vpc-id,Values=$vpcId" \
                  "Name=group-name,Values=$SECURITY_GROUP_NAME" \
        --query "SecurityGroups[*].GroupId" \
        --output text)

if [[ -z $sgId ]]; then
  echo "[*] Creating $SECURITY_GROUP_NAME security group"
  sgId=$(aws ec2 create-security-group \
          --group-name $SECURITY_GROUP_NAME \
          --description "$SECURITY_GROUP_DESCRIPTION" \
          --vpc-id $vpcId \
          --query GroupId \
          --output text)
fi

echo "$SECURITY_GROUP_NAME security group id: $sgId"

MomPopCafeSecurityGroupId=$(aws ec2 describe-security-groups \
                              --filters "Name=vpc-id,Values=$vpcId" \
                                        "Name=tag:Name,Values=MomPopCafeSecurityGroup" \
                              --query "SecurityGroups[*].GroupId" \
                              --output text)

echo "MomPopCafeSecurityGroup ID: $MomPopCafeSecurityGroupId"

aws ec2 authorize-security-group-ingress \
  --group-id $sgId \
  --protocol tcp --port 3306 \
  --source-group $MomPopCafeSecurityGroupId 2> /dev/null

echo "Describing $SECURITY_GROUP_NAME security group:"
aws ec2 describe-security-groups \
  --filters "Name=group-name,Values=$SECURITY_GROUP_NAME" \
  --query "SecurityGroups[*].{GroupName: GroupName,
                                GroupId: GroupId,
                                IpPermissions: IpPermissions}" \
  --no-paginate --no-cli-pager


# Creating private subnets
instanceAZ=$(aws ec2 describe-instances \
              --filters "Name=tag:Name,Values=$WEBSERVER_NAME" \
                        "Name=instance-state-name,Values=running" \
              --query "Reservations[*].Instances[*].Placement.AvailabilityZone" \
              --output text)

_size=${#instanceAZ}
region=${instanceAZ:0:$((_size-1))}
echo "Instance region: $region"

AZs=$(aws ec2 describe-availability-zones \
        --region $region \
        --query 'AvailabilityZones[].ZoneName' \
        --output text) \

echo "AZs $region: $AZs"

# removing instance AZ from region AZs 
# (to select a different AZ for Private Subnet 2)
differentAZ=$(echo $AZs | sed 's/ /\n/g' |grep -vE "$instanceAZ" | head -n1)

echo "[DEBUG] - differentAZ: $differentAZ"

PRIVATE_SUBNETS_AZS=($instanceAZ  $differentAZ)
echo "[DEBUG] PRIVATE_SUBNETS_AZS: ${PRIVATE_SUBNETS_AZS[@]}"

subnetIds=()
n=${#PRIVATE_SUBNETS_NAMES[@]}

for i in $(seq 0 $((n-1)));do
  subnetName=${PRIVATE_SUBNETS_NAMES[i]}
  subnetCidr=${PRIVATE_SUBNETS_CIDRS[i]}
  az=${PRIVATE_SUBNETS_AZS[i]}

  subnetId=$(aws ec2 describe-subnets \
                --filters "Name=vpc-id,Values=$vpcId" \
                          "Name=cidr-block,Values=$subnetCidr" \
                          "Name=tag:Name,Values=$subnetName" \
                --query "Subnets[].SubnetId" \
                --output text)

  if [[ -z $subnetId ]];then
    echo "Creating subnet $subnetCidr on $az (Name: $subnetName)"
    subnetId=$(aws ec2 create-subnet \
                --vpc-id $vpcId \
                --cidr-block $subnetCidr \
                --availability-zone $az \
                --tag-specifications "ResourceType=subnet,
                                      Tags=[{Key=Name,Value=$subnetName}]" \
                --query 'Subnet.SubnetId' \
                --output text)
  fi

  subnetIds[i]=$subnetId
  echo "subnetIds: ${subnetIds[@]}"
done

# Creating db subnet group

dbSubnetGroupName=$(aws rds describe-db-subnet-groups \
                    --db-subnet-group-name $SUBNET_GROUP_NAME \
                    --query 'DBSubnetGroups[].DBSubnetGroupName' \
                    --output text 2>/dev/null)

if [[ -z $dbSubnetGroupName ]];then
  echo "[*] Creating $SUBNET_GROUP_NAME DB subnet group" 
  aws rds create-db-subnet-group \
    --db-subnet-group-name $SUBNET_GROUP_NAME \
    --db-subnet-group-description "$SUBNET_GROUP_DESCRIPTION" \
    --subnet-ids ${subnetIds[@]} \
    --tags "Key=Name,Value= MomPopCafeDatabaseSubnetGroup" \
    --no-paginate --no-cli-pager
fi
