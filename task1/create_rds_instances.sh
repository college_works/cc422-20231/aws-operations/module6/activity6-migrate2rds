#!/bin/bash
#
# Student: Lozano Gustavo - 20161317K
#
# This script automate the following subtask:
#  - "Task 1.4: Create the Amazon RDS MariaDB instance"

#### RDS INSTANCE
# DB instance identifier: MomPopCafeDBInstance
# Engine option: MariaDB
# DB engine version: 10.2.11
# DB instance class: db.t2.micro
# Allocated storage: 20 GB
# Availability Zone: MomPopCafeInstance Availability Zone
# DB Subnet group: MomPopCafeDB Subnet Group
# VPC security groups: MomPopCafeDatabaseSG
# Public accessibility: No
# Username: root
# Password: Re:Start!9

source vars.sh

DB_INSTANCE_IDENTIFIER='MomPopCafeDBInstance'
ENGINE_NAME='mariadb'
ENGINE_VERSION='10.6.10'
DB_INSTANCE_TYPE='db.t2.micro'
ALLOCATED_STORAGE=20

DB_USER='root'
DB_PASSWORD='Re:Start!9'

# Getting instance AZ
instanceAZ=$(aws ec2 describe-instances \
              --filters "Name=tag:Name,Values=$WEBSERVER_NAME" \
              --query "Reservations[*].Instances[*].Placement.AvailabilityZone" \
              --output text)

# Getting VPC id
vpcId=$(aws ec2 describe-vpcs \
          --filter "Name=tag:Name,Values=$VPC_NAME" \
          --query "Vpcs[*].VpcId" \
          --output text)

echo "$VPC_NAME vpc: $vpcId"

# Getting database security group ID
sgId=$(aws ec2 describe-security-groups \
        --filters "Name=vpc-id,Values=$vpcId" \
                  "Name=group-name,Values=$SECURITY_GROUP_NAME" \
        --query "SecurityGroups[*].GroupId" \
        --output text)

if [[ -z $sgId ]]; then
  echo "[*] Creating $SECURITY_GROUP_NAME security group"
  sgId=$(aws ec2 create-security-group \
          --group-name $SECURITY_GROUP_NAME \
          --description "$SECURITY_GROUP_DESCRIPTION" \
          --vpc-id $vpcId \
          --query GroupId \
          --output text)
fi

echo "$SECURITY_GROUP_NAME security group id: $sgId"


# Creating DB instance
echo "[*] Creating $DB_INSTANCE_IDENTIFIER DB instance"
aws rds create-db-instance \
    --db-instance-identifier $DB_INSTANCE_IDENTIFIER \
    --engine $ENGINE_NAME \
    --engine-version $ENGINE_VERSION \
    --db-instance-class $DB_INSTANCE_TYPE \
    --allocated-storage $ALLOCATED_STORAGE \
    --availability-zone $instanceAZ \
    --db-subnet-group-name $SUBNET_GROUP_NAME \
    --vpc-security-group-ids $sgId \
    --no-publicly-accessible \
    --master-username $DB_USER --master-user-password $DB_PASSWORD \
    | grep -E "DBInstanceIdentifier|DBInstanceClass|Engine|DBInstanceStatus|AvailabilityZone" 2> /dev/null

echo "Waiting for $DB_INSTANCE_IDENTIFIER DB ..."
aws rds wait db-instance-available \
  --db-instance-identifier $DB_INSTANCE_IDENTIFIER


# Describing DB instances status
aws rds describe-db-instances \
--db-instance-identifier $DB_INSTANCE_IDENTIFIER \
--query "DBInstances[*].
              {EndpointAddress: Endpoint.Address,
                AvailabilityZone: AvailabilityZone,
                PreferredBackupWindow: PreferredBackupWindow,
                BackupRetentionPeriod: BackupRetentionPeriod,
                DBInstanceStatus: DBInstanceStatus}"