#!/bin/bash
#
# Student: Lozano Gustavo - 20161317K
#
# This script automate the following subtask:
#  - "Task 1.2: Gather current environment configuration information"
# which gatter information about AWS enviroment before the migration.
#
#
# Requirements: ['jq', ]


WEBSERVER_NAME='MomPopCafeInstance'
VPC_NAME='MomPopCafe VPC'

echo "[*] Describing $WEBSERVER_NAME instance:"

instanceDescription=$(aws ec2 describe-instances \
                        --filters "Name=tag:Name,Values=$WEBSERVER_NAME" \
                        --query "Reservations[].Instances[0].\
                                  {InstanceId: InstanceId, InstanceType: InstanceType, 
                                    PublicDnsName: PublicDnsName, PublicIpAddress: PublicIpAddress, 
                                    AvailabilityZone: Placement.AvailabilityZone, 
                                    VpcId: VpcId, SecurityGroupIds: SecurityGroups[*].GroupId}" \
                        --output json | jq '.[]')

echo $instanceDescription | jq

vpcId=$(echo $instanceDescription | jq -r '.VpcId')
echo "$VPC_NAME id: $vpcId"

cidr=$(aws ec2 describe-vpcs --vpc-ids $vpcId \
        --filters "Name=tag:Name,Values=$VPC_NAME" \
        --query "Vpcs[*].CidrBlock" \
        --output text)

echo "$VPC_NAME IPv4 CIDR block: $cidr"

echo "subnet of vpc $VPC_NAME:"
aws ec2 describe-subnets \
  --filters "Name=vpc-id,Values=$vpcId" \
  --query "Subnets[*].
              {SubnetId: SubnetId, 
                CidrBlock: CidrBlock, 
                SubnetName: Tags[?Key=='Name'].Value}"


instanceAZ=$(echo $instanceDescription | jq -r '.AvailabilityZone')

_size=${#instanceAZ}
region=${instanceAZ:0:$((_size-1))}
echo "Instance region: $region"

echo "AZs $region: $AZs"
aws ec2 describe-availability-zones \
 --filters "Name=region-name,Values=$region" \
 --query "AvailabilityZones[*].ZoneName"


WEBSITE_URL="http://$(echo $instanceDescription | jq -r '.PublicDnsName')/mompopcafe/orderHistory.php"
echo "Website URL: $WEBSITE_URL"
numberOrders=$(curl -s $WEBSITE_URL | grep -Eo "Order Number\: ([[:digit:]]*)" | grep -o "[[:digit:]]*$")

echo "Number of orders: $numberOrders"