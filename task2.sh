#!/bin/bash
#
# Student: Lozano Gustavo - 20161317K
#
# This script automate the following subtask:
#  - "Task 2: Migrate application data to the Amazon RDS instance"

source vars.sh

if [[ $# < 1 ]];then
  echo "USAGE: $0 RDS_DB_ENDPOINT"
fi

DB_NAME='mom_pop_db'
DUMP_FILE='mompopdb-backup.sql'

echo "Dumping $DB_NAME database to $DUMP_FILE"
mysqldump --user=$DB_USER --password=$DB_PASSWORD \
  --databases $DB_NAME --add-drop-database > $DUMP_FILE

echo "HEAD of $DUMP_FILE file"
head -n 10 $DUMP_FILE

rdsEndpoint=$1

echo "Restore $DUMP_FILE on $rdsEndpoint endpoint"

mysql --user=$DB_USER --password=$DB_PASSWORD \
  --host=$rdsEndpoint < $DUMP_FILE

echo "Verifying migrations to RDS"
mysql --user=$DB_USER --password=$DB_PASSWORD \
  --host=$rdsEndpoint  -D $DB_NAME -e "select * from product LIMIT 5;"
