# Variables file - Task 1

## VARIABLES - Task 1.2: Gather current environment configuration information

WEBSERVER_NAME='MomPopCafeInstance'
VPC_NAME='MomPopCafe VPC'


## VARIABLES - Task 1.3: Create prerequisite components
SECURITY_GROUP_NAME="MomPopCafeDatabaseSG"
SECURITY_GROUP_DESCRIPTION="Security group for Mom Pop Cafe database"
PRIVATE_SUBNETS_NAMES=('MomPopCafeDB Private Subnet 1'
                        'MomPopCafeDB Private Subnet 2')
PRIVATE_SUBNETS_CIDRS=('10.200.2.0/23' '10.200.10.0/23')
SUBNET_GROUP_NAME="MomPopCafeDB-Subnet-Group"
SUBNET_GROUP_DESCRIPTION="DB subnet group for Mom & Pop Cafe"


## VARIABLES - Task 1.4: Create the Amazon RDS MariaDB instance

DB_INSTANCE_IDENTIFIER='MomPopCafeDBInstance'
ENGINE_NAME='mariadb'
ENGINE_VERSION='10.6.10'
DB_INSTANCE_TYPE='db.t2.micro'
ALLOCATED_STORAGE=20

DB_USER='root'
DB_PASSWORD='Re:Start!9'